from django.contrib import admin
from apps.models import client, lunette, commande

admin.site.register(client)
admin.site.register(lunette)
admin.site.register(commande)
