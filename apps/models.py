from django.db import models

# Create your models here.
class client (models.Model):
    nom = models.CharField(max_length=30, blank=False, null=False)
    prenom = models.CharField(max_length=30, blank=False, null=False)
    adresse = models.CharField(max_length=30, blank=False, null=False)
    telephon = models.CharField(max_length=30, blank=False, null=False)
    photo = models.FileField(max_length=500, blank=False, null=False)
    def __str__(self): 
		        return str(self.nom) +' '

class lunette (models.Model):
    nom = models.CharField(max_length=30, blank=False, null=False)
    type = models.CharField(max_length=30, blank=False, null=False)
    prix = models.CharField(max_length=30, blank=False, null=False)
    photo = models.FileField(max_length=500, blank=False, null=False)
    def __str__(self): 
		        return str(self.nom) +' '

class commande (models.Model):
    id_client = models.ForeignKey(client, on_delete=models.CASCADE)
    id_lunette = models.ForeignKey(lunette, on_delete=models.CASCADE)
    date = models.CharField(max_length=30, blank=False, null=False)
    nbre_lunettes = models.CharField(max_length=30, blank=False, null=False)
    montantTotal =models.FloatField()
    def __str__(self): 
		        return str(self.montantTotal) +' '
