from rest_framework import serializers
from apps.models import client, lunette, commande

class clientSerializer(serializers.ModelSerializer):  
	class Meta:
	    model = client
	    fields = '__all__'

class lunetteSerializer(serializers.ModelSerializer):
    class Meta:
	    model = lunette
	    fields = '__all_'
	
class commandeSerializer(serializers.ModelSerializer):
	class Meta:
	    model = commande
	    fields = '__all_'