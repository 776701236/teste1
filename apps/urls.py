from django.urls import path
from django.conf.urls import url, include
from . import views


urlpatterns = [
    path('', views.index, name='index'),
    url(r'^client/$', views.clientListCreateView.as_view()),
    url(r'^clientupdate/(?P<pk>[0-9]+)/$', views.clientUpdateView.as_view()),
    url(r'^clientdelete/(?P<pk>[0-9]+)/$', views.clientDeleteView.as_view()),
]
