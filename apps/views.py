from django.shortcuts import render
from django.http import HttpResponse
from apps.serializers import *
from apps.models import *
from rest_framework import generics, permissions, status
from rest_framework.response import Response


# Create your views here.
def index(request):
    return HttpResponse("Bonjour c'est sidy sogue")

class clientListCreateView(generics.CreateAPIView):
	serializer_class = clientSerializer
	def get(self, request, *args, **kwargs):
		client = Client.objects.all()

		if not client:
			return Response({
				"status": "failure",
				"message": "no such item.",
				}, status=status.HTTP_404_NOT_FOUND)
			serializer = clientSerializer(client, many=True) 

			return Response({
				"status": "success",
				"message": "items successfully retrieved",
				"count": client.count(),
				"data": serializer.data
				}, status = status.HTTP_200_OK)   
	def post(self, request, *args, **kwargs):
		serializer = clientSerializer(data = request.data)
		if not serializer.is_valid():
			return Response({
			    "status": "faillure",
			    "message": serializer.errors,
			    "error": "erreur"

			 }, status=status.HTTP_404_BAD_REQUEST)
			serializer.save()
			return Response({
				"status": "success",
				"message": "item successfully created",

			}, status=status.HTTP_201_CREATED)    
				
class clientUpdateView(generics.CreateAPIView):

	def put(self, request, *args, **kwargs):
		try:
			client = client.objects.get(id=kwargs['pk'])
		except client.DoesNotExist:
			return Response({
				"status": "failure",
				"message": "no such item"
				}, status=status.HTTP_400_BAD_REQUEST)

		client_data = {
		    "name": request.data['name'],
			"description": request.data['description'],
		}

		serializer = clientSerializer(client, data=client_data, partial=True)

		if not serializer.is_valid():
			return Response({
				"status": "failure",
				"message": "invalid data",
				"errors": serializer.errors
			}, status=status.HTTP_400_BAD_REQUEST)

		serializer.save()
		return Response({
			"status": "success",
			"message": "item successfully update",
			"data": serializer.data
			},status=status.HTTP_200_OK)


class clientDeleteView(generics.CreateAPIView):

	def post(self, request, *args, **kwargs):
		try:
			client = client.objects.get(id=kwargs['pk'])
		except client.DoesNotExist:
			return Response({
				"status":"failure",
				"message": "no such item"
			}, status=status.HTTP_400_BAD_REQUEST)

		client.delete()
		return Response({
			"status=":"success",
			"message": "client successfully delete"
			}, status=status.HTTP_200_OK)


